package cn.bootx.common.core.code.actable;

/**
 * mysql支持的引擎
 */
public enum MySqlEngine {

    DEFAULT, ARCHIVE, BLACKHOLE, CSV, InnoDB, MEMORY, MRG_MYISAM, MyISAM, PERFORMANCE_SCHEMA;

}
